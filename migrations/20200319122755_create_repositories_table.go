package migrations

import migrate "github.com/rubenv/sql-migrate"

func init() {
	m := &migrate.Migration{
		Id: "20200319122755_create_repositories_table",
		Up: []string{
			`CREATE TABLE IF NOT EXISTS repositories (
                id bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY,
                parent_id bigint,
                created_at timestamp WITH time zone NOT NULL DEFAULT now(),
    			updated_at timestamp WITH time zone,
                name text NOT NULL,
                path text NOT NULL,
                CONSTRAINT pk_repositories PRIMARY KEY (id),
                CONSTRAINT fk_repositories_parent_id_repositories FOREIGN KEY (parent_id) REFERENCES repositories (id) ON DELETE CASCADE,
                CONSTRAINT uq_repositories_path UNIQUE (path),
                CONSTRAINT ck_repositories_name_length CHECK ((char_length(name) <= 255)),
                CONSTRAINT ck_repositories_path_length CHECK ((char_length(path) <= 255))
            )`,
			"CREATE INDEX IF NOT EXISTS ix_repositories_parent_id ON repositories (parent_id)",
		},
		Down: []string{
			"DROP INDEX IF EXISTS ix_repositories_parent_id CASCADE",
			"DROP TABLE IF EXISTS repositories CASCADE",
		},
	}

	allMigrations = append(allMigrations, m)
}
